# -*- coding:utf-8 -*-
__author__ = """David Scheliga"""
__email__ = "david.scheliga@gmx.de"
__version__ = "0.1.0"

def smile():
    """
    >>> smile()
    ':)'
    """
    return ":)"
